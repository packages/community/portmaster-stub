# Maintainer: Bernhard Landauer <bernhard@manjaro.org>
# Maintainer: Mark Wagie <mark at manjaro dot org>
# Contributor: Safing ICS Technologies <noc@safing.io>
#
# Application Firewall: Block Mass Surveillance - Love Freedom
# The Portmaster enables you to protect your data on your device. You
# are back in charge of your outgoing connections: you choose what data
# you share and what data stays private. Read more on docs.safing.io.
#
pkgname=portmaster-stub
pkgver=1.0.13
pkgrel=1
pkgdesc="Privacy Suite and Firewall: Installer to download the current binaries"
arch=('x86_64')
url="https://safing.io/portmaster"
license=('AGPL-3.0-or-later')
depends=('gtk3' 'hicolor-icon-theme' 'nss' 'systemd-libs')
makedepends=('imagemagick')
optdepends=('libappindicator-gtk3: for systray indicator'
            'networkmanager: recommended for better integration')
options=('!strip')
provides=('portmaster')
conflicts=('portmaster')
replaces=('portmaster-stub-bin')
install='portmaster.install'
source=("https://updates.safing.io/linux_amd64/start/portmaster-start_v${pkgver//./-}"
        "https://raw.githubusercontent.com/safing/portmaster-packaging/v${pkgver}/linux/portmaster.desktop"
        "https://raw.githubusercontent.com/safing/portmaster-packaging/v${pkgver}/linux/portmaster_notifier.desktop"
        "https://raw.githubusercontent.com/safing/portmaster-packaging/v${pkgver}/linux/portmaster_logo.png"
        "https://raw.githubusercontent.com/safing/portmaster-packaging/v${pkgver}/linux/portmaster.service")
sha256sums=('8932bf3ccc057af8db6ff623ddc27024e9821c40f82729e77e8b0541c0f22fd9'
            '7b0c03e4552dd86caeff2d628b13346cfe70a646af11abac6555e348e46c28da'
            '490b586f185218fdd947e8f12aa2dc412d78d89c8ce9b8ef5a75cb2e5ffb94ae'
            'ecb02625952594af86d3b53762363c1e227c2b9604fc9c9423682fc87a92a957'
            'bc26dd37e6953af018ad3676ee77570070e075f2b9f5df6fa59d65651a481468')

prepare() {
  for res in 16 32 48 96 128 ; do
    local iconpath="icons/${res}x${res}/"
    mkdir -p "${iconpath}" ;
    convert ./portmaster_logo.png -resize "${res}x${res}" "${iconpath}/portmaster.png" ;
  done
}

package() {
  install -Dm755 "portmaster-start_v${pkgver//./-}" \
    "${pkgdir}/opt/safing/portmaster/portmaster-start"
  install -Dm644 portmaster.desktop -t "${pkgdir}/usr/share/applications/"
  install -Dm644 portmaster_notifier.desktop -t "${pkgdir}/etc/xdg/autostart/"
  install -Dm644 portmaster.service -t "${pkgdir}/usr/lib/systemd/system/"

  for icon_size in 16 32 48 96 128; do
  install -Dm644 "icons/${icon_size}x${icon_size}/portmaster.png" -t \
    "${pkgdir}/usr/share/icons/hicolor/${icon_size}x${icon_size}/apps/"
  done
}
